Source: coinor-bonmin
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Julien Schueller <schueller@phimeca.com>,
           Pierre Gruet <pgt@debian.org>
Section: science
Priority: optional
Build-Depends: dpkg-dev (>= 1.22.5), debhelper-compat (= 10),
               coinor-libcbc-dev,
#               coinor-libcgl-dev,
#               coinor-libclp-dev,
               coinor-libcoinutils-dev,
               coinor-libipopt-dev (>= 3.14.17-2),
#               coinor-libosi-dev,
               libamplsolver-dev,
#               liblapack-dev,
#               libblas-dev,
               libbz2-dev,
               zlib1g-dev,
               doxygen,
               graphviz,
               pkgconf,
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/science-team/coinor-bonmin
Vcs-Git: https://salsa.debian.org/science-team/coinor-bonmin.git
Homepage: https://projects.coin-or.org/Bonmin

Package: coinor-libbonmin4t64
X-Time64-Compat: coinor-libbonmin4
Provides: ${t64:Provides}
Replaces: coinor-libbonmin4
Breaks: coinor-libbonmin4 (<< ${source:Version})
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Multi-Arch: same
Description: COIN-OR mixed integer programming
 Bonmin (Basic Open-source Nonlinear Mixed INteger programming) is an
 open-source code for solving general MINLP (Mixed Integer NonLinear
 Programming) problems.
 .
 Bonmin is part of the larger COIN-OR initiative (Computational Infrastructure
 for Operations Research) and can be used with other COIN-OR packages that
 make use of cuts, such as the mixed-integer linear programming solver Cbc.
 .
 This package contains the binaries and libraries.

Package: coinor-libbonmin-dev
Architecture: any
Section: libdevel
Depends: coinor-libbonmin4t64 (= ${binary:Version}),
         coinor-libcbc-dev,
         coinor-libipopt-dev,
         ${misc:Depends}
Multi-Arch: same
Description: COIN-OR mixed integer programming (developer files)
 Bonmin (Basic Open-source Nonlinear Mixed INteger programming) is an
 open-source code for solving general MINLP (Mixed Integer NonLinear
 Programming) problems.
 .
 Bonmin is part of the larger COIN-OR initiative (Computational Infrastructure
 for Operations Research) and can be used with other COIN-OR packages that
 make use of cuts, such as the mixed-integer linear programming solver Cbc.
 .
 This package contains the header files for developers.
